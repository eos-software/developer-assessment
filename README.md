# Developer Assessment
<img src="Resources/logo.png" width="175px" alt="EOS Software Logo">

We are looking for members who are interested in being part of an agile, progressive, proactive team with new ideas.

We seek to use technology as an ally to link our objectives as well as to offer customers a better user experience as well as delivering added value in our products.

### Assessment:
* [Assestment](assessments/index.md)
