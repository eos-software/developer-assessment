# Assessment

### Cash Machine

Create a model to represent a User with the follow use cases:
* A user can have "N" accounts
* An account could be "credit" or "debit" type

* Initialize the user debit account with $1,000.00 amount
* A user can withdraw from debit account only if the user has the available amount otherwise thrown an error message
* A user can deposit to his debit account

* Initialize the user credit account with $1,000.00 amount
* A user can withdraw from credit account with 10% fee only if the user has credit otherwise thrown an error message
* A user can pay for his credit account only 

#### Note: feel free to design the ER model their relationships and attributes

## Requirement
* File README.md with instructions to run the project
### Backend Requirements
* Node, Typescript
* Database MySQL or MariaDB 
* A modern Framework Node (Express, NestJs etc...)

### Differential (Desirable), it would be great if you could use the next but not mandatory
* RestFul API
* Unit tests 
* Containers (Docker)
* CI pipelines
* Gitflow

### Evaluation criteria
* Creativity: The previous instructions do not limit any desire of the developer, feel free to propose
* Organization: project structure, versioning
* Good practices, Standards (PSRs, Linteners, etc)
* Technology: use of paradigms, frameworks and libraries
* Design patterns, OOP, Functional Programming, S.O.L.I.D. principles
